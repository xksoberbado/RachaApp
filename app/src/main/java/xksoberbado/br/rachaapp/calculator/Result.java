package xksoberbado.br.rachaapp.calculator;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import xksoberbado.br.rachaapp.model.Participant;

@AllArgsConstructor
@Getter
@Setter
public class Result {

    private Participant participant;

    private BigDecimal total;

}
