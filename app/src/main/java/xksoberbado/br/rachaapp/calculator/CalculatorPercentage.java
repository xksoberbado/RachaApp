package xksoberbado.br.rachaapp.calculator;

import java.math.BigDecimal;

import xksoberbado.br.rachaapp.model.Purchase;

public class CalculatorPercentage extends CalculatorSimple {

    public CalculatorPercentage(Purchase purchase) {
        super(purchase);
    }

    @Override
    public void execute() {
        super.execute();
        for(Result result : results){
            BigDecimal valuePlus = result.getTotal().multiply(purchase.getPercent());
            result.setTotal(result.getTotal().add(valuePlus));
        }
    }
}
