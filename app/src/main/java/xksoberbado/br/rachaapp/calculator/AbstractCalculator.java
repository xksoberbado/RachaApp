package xksoberbado.br.rachaapp.calculator;

import java.util.Collection;
import java.util.LinkedList;

import xksoberbado.br.rachaapp.model.Purchase;

public abstract class AbstractCalculator implements ICalculator{

    protected Purchase purchase;
    protected Collection<Result> results;

    public AbstractCalculator(Purchase purchase) {
        this.purchase = purchase;
        this.results = new LinkedList<>();
        execute();
    }

    @Override
    public Collection<Result> getResults() {
        return results;
    }
}
