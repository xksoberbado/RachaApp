package xksoberbado.br.rachaapp.calculator;

import java.util.Collection;

/**
 * Created by Igor on 08/07/2018.
 */

public interface ICalculator {

    void execute();

    Collection<Result> getResults();
}
