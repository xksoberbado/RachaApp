package xksoberbado.br.rachaapp.calculator;

import java.math.BigDecimal;

import xksoberbado.br.rachaapp.model.Purchase;

public class CalculatorFactory {

    private static CalculatorFactory instance;

    private CalculatorFactory() {
    }

    public synchronized static CalculatorFactory getInstance() {
        if(instance == null)
            instance = new CalculatorFactory();

        return instance;
    }

    public ICalculator get(Purchase purchase){
        BigDecimal percentage = purchase.getPercentage();
        if(percentage != null && percentage.doubleValue() > 0){
            return new CalculatorPercentage(purchase);
        } else {
            return new CalculatorSimple(purchase);
        }
    }
}
