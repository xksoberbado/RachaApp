package xksoberbado.br.rachaapp.helper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import xksoberbado.br.rachaapp.calculator.CalculatorFactory;
import xksoberbado.br.rachaapp.calculator.Result;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.model.Participant;

/**
 * Created by Igor on 08/07/2018.
 */

public class ReceiptHelper {

    private static Canvas canvas;
    private static Paint paint;
    private static float text_x = 50;
    private static float text_y = 50;

    private ReceiptHelper() {
    }

    public static Bitmap generate(Purchase purchase) {
        Collection<Result> results = CalculatorFactory.getInstance().get(purchase).getResults();
        int qttItems = purchase.getItems().size();
        int qttResults = results.size();
        int size = 3 + qttItems  + qttResults + 1;
        Bitmap bitMap = Bitmap.createBitmap(500, (size) * 50,  Bitmap.Config.ARGB_8888);
        bitMap.eraseColor(Color.rgb(251, 236, 93));
        canvas = new Canvas(bitMap);

        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(25);

        addLine(addIfens("ITENS COMPRA"));

        for(PurchaseItem item : purchase.getItems())
            addLine(item.getName()+ " - "+ item.getTotal());

        addLine("Total: R$ "+ purchase.getTotal());
        addLine(addIfens("DIVISÃO"));

        for(Result r : results){
            addLine(r.getParticipant().getName() + " - "+ r.getTotal());
        }

        return bitMap;
    }

    private static void addLine(String text){
        canvas.drawText(text, text_x, text_y, paint);
        text_y = text_y + 50;
    }

    private static String addIfens(String text){
        return "-------------- "+text+" --------------";
    }
}
