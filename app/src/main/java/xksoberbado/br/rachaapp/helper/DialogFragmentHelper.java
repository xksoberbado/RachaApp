package xksoberbado.br.rachaapp.helper;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import xksoberbado.br.rachaapp.arq.App;

public class DialogFragmentHelper {

    private DialogFragmentHelper() {
    }

    public static void showFragmentDialog(DialogFragment fragment, String tag) {
        fragment.show(App.getSupportFragmentManager(), tag);
    }

    public static void dismissByTag(String tag){
        Fragment fragment = App.getSupportFragmentManager().findFragmentByTag(tag);
        if(fragment != null){
            DialogFragment dialogFragment = (DialogFragment) fragment;
            dialogFragment.dismiss();
        }
    }

}
