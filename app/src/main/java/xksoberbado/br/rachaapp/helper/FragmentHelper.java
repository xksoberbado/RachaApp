package xksoberbado.br.rachaapp.helper;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.R;

public class FragmentHelper {

    private FragmentHelper() {
    }

    public static void replaceFragment(Fragment fgmt){
        FragmentTransaction ft = App.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_content, fgmt);
        ft.commit();
    }

    public static void refreshFragment(int id){
        Fragment fgmt = App.getSupportFragmentManager().findFragmentById(id);
        FragmentTransaction ft = App.getSupportFragmentManager().beginTransaction();
        ft.detach(fgmt);
        ft.attach(fgmt);
        ft.commit();
    }

}
