package xksoberbado.br.rachaapp.helper;

import android.view.View;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;

import xksoberbado.br.rachaapp.R;

public class ButtonHelper {

    private ButtonHelper() {
    }

    public static void setButtonCancelDialog(View viewWithButton, final DialogFragment dialog){
        Button btnCancel = viewWithButton.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

}
