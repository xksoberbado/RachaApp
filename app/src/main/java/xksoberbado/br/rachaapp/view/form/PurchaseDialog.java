package xksoberbado.br.rachaapp.view.form;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.math.BigDecimal;
import java.util.Date;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.helper.FormHelper;
import xksoberbado.br.rachaapp.view.fragment.PurchasesFragment;
import xksoberbado.br.rachaapp.helper.ButtonHelper;
import xksoberbado.br.rachaapp.helper.FragmentHelper;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.repository.PurchaseDao;

/**
 * Created by igor.rudel on 03/07/2018.
 */

public class PurchaseDialog extends DialogFragment {

    private Purchase purchase;
    private EditText etName;
    private EditText etPercentage;
    private BigDecimal percentage;

    public PurchaseDialog() {
    }

    public PurchaseDialog(Purchase purchase) {
        this.purchase = purchase;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.form_purchase, null);
        etName = fragment.findViewById(R.id.et_name);

        TextView tvPercentage = fragment.findViewById(R.id.tv_default);
        tvPercentage.setText(getString(R.string.percentage_purchase));
        etPercentage = fragment.findViewById(R.id.et_percentage);

        if(!isNewPurchase()){
            etName.setText(purchase.getName());
            if(purchase.getPercentage() != null)
                etPercentage.setText(purchase.getPercentage().toString());
        }

        Button btnSave = fragment.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    PurchaseDao dao = new PurchaseDao();
                    String name = etName.getText().toString();
                    if(isNewPurchase()){
                        dao.create(new Purchase(new Date(), name, percentage), R.string.purchase_created);
                    } else {
                        purchase.setName(name);
                        purchase.setPercentage(percentage);
                        dao.update(purchase, R.string.purchase_updated);
                    }

                    FragmentHelper.replaceFragment(new PurchasesFragment());
                    dismiss();
                }
            }
        });

        ButtonHelper.setButtonCancelDialog(fragment, this);

        return fragment;
    }

    private boolean isNewPurchase(){
        return purchase == null;
    }

    private boolean validate(){
        boolean validate = FormHelper.validateEt(etName);

        percentage = FormHelper.validateEt(etPercentage) ? new BigDecimal(etPercentage.getText().toString()) : null;

        return validate;
    }
}
