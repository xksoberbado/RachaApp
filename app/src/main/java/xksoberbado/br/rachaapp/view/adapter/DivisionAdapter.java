package xksoberbado.br.rachaapp.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.calculator.Result;
import xksoberbado.br.rachaapp.helper.MoneyHelper;

/**
 * Created by Igor on 04/07/2018.
 */

public class DivisionAdapter extends RecyclerView.Adapter<DivisionAdapter.ViewLine> {

    private ArrayList<Result> results;

    public DivisionAdapter(ArrayList<Result> results) {
        this.results = results;
    }

    @NonNull
    @Override
    public ViewLine onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.division_line, parent, false);
        return new ViewLine(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewLine holder, int position) {
        Result r = results.get(position);
        holder.tvName.setText(r.getParticipant().getName());
        holder.tvTotal.setText("R$ "+ MoneyHelper.setTwoDecimalPlaces(r.getTotal()));
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewLine extends RecyclerView.ViewHolder{

        public TextView tvName;
        public TextView tvTotal;

        public ViewLine(@NonNull View itemView) {
            super(itemView);
            this.tvName = itemView.findViewById(R.id.tv_name);
            this.tvTotal = itemView.findViewById(R.id.tv_total);
        }
    }
}
