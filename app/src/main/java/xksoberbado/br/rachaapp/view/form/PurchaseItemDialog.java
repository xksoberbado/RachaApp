package xksoberbado.br.rachaapp.view.form;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.math.BigDecimal;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.arq.helper.FormHelper;
import xksoberbado.br.rachaapp.arq.helper.SnackBarHelper;
import xksoberbado.br.rachaapp.view.fragment.PurchaseItemsFragment;
import xksoberbado.br.rachaapp.helper.ButtonHelper;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.repository.PurchaseItemDao;

/**
 * Created by Igor on 03/07/2018.
 */

public class PurchaseItemDialog extends DialogFragment {

    private Purchase purchase;
    private PurchaseItem item;
    private EditText etName;
    private EditText etQuantity;
    private EditText etPrice;
    private Button btnSave;

    private PurchaseItemDialog() {
    }

    public PurchaseItemDialog(Purchase purchase) {
        this.purchase = purchase;
    }

    public PurchaseItemDialog(PurchaseItem item) {
        this.item = item;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.form_purchase_item, null);
        etName = fragment.findViewById(R.id.et_name);
        etQuantity = fragment.findViewById(R.id.et_quantity);
        etPrice = fragment.findViewById(R.id.et_price);

        btnSave = fragment.findViewById(R.id.btn_save);

        if(!isNewItem()){
            etName.setText(item.getName());
            etPrice.setText(item.getPrice().toString());
            etQuantity.setText(item.getQuantity().toString());
        } else {
            etQuantity.setText("1");
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    String name = etName.getText().toString();
                    BigDecimal price = new BigDecimal(etPrice.getText().toString());
                    Integer quantity = new Integer(etQuantity.getText().toString());
                    PurchaseItemDao dao = new PurchaseItemDao();
                    if(isNewItem()){
                        dao.create(new PurchaseItem(name, quantity, price, purchase));
                        etName.setText("");
                        etPrice.setText("");
                        etQuantity.setText("");
                        etName.requestFocus();
                        Toast.makeText(getContext(), R.string.purchase_item_created, Toast.LENGTH_SHORT).show();
                    } else {
                        item.setName(name);
                        item.setPrice(price);
                        item.setQuantity(quantity);
                        dao.update(item);
                        dismiss();
                        SnackBarHelper.success(R.string.purchase_item_updated);
                    }
                    App.sendBroadcast(PurchaseItemsFragment.REFRESH);
                }
            }
        });

        ImageButton ibMinus = fragment.findViewById(R.id.ib_minus);
        ibMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventIB(false);
            }
        });

        ImageButton ibPlus = fragment.findViewById(R.id.ib_plus);
        ibPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventIB(true);
            }
        });

        ButtonHelper.setButtonCancelDialog(fragment, this);

        return fragment;
    }

    private void eventIB(boolean plus){
        Integer qtt = new Integer(etQuantity.getText().toString());
        if(plus){
            qtt++;
        } else if (qtt > 1){
            qtt--;
        }
        etQuantity.setText(qtt.toString());
    }

    private boolean isNewItem(){
        return item == null;
    }

    private boolean validate(){
        boolean validate = FormHelper.validateEts(etName, etPrice, etQuantity);
        BigDecimal price = new BigDecimal(etPrice.getText().toString());
        Integer qtt = new Integer(etQuantity.getText().toString());
        if(validate && price.doubleValue() > 0 && qtt > 0)
            return true;
        else {
            if(price.doubleValue() <= 0)
                etPrice.setError(getString(R.string.greater_than_zero));

            if(qtt <= 0)
                etQuantity.setError(getString(R.string.greater_than_zero));

            return false;
        }
    }
}
