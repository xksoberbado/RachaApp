package xksoberbado.br.rachaapp.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xksoberbado.br.rachaapp.calculator.CalculatorFactory;
import xksoberbado.br.rachaapp.calculator.Result;
import xksoberbado.br.rachaapp.view.adapter.DivisionAdapter;
import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.helper.FragmentHelper;
import xksoberbado.br.rachaapp.helper.MoneyHelper;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.model.Participant;

/**
 * Created by igor.rudel on 04/07/2018.
 */

public class DivisionFragment extends Fragment {

    private Purchase purchase;
    private Collection<Result> results;

    private DivisionFragment() {
    }

    public DivisionFragment(Purchase purchase) {
        setHasOptionsMenu(true);
        this.purchase = purchase;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                FragmentHelper.replaceFragment(new PurchasesFragment());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Collection<PurchaseItem> items = purchase.getItems();
        String itemOuItens = items.size() > 1 ? " Itens = " : " Item = ";
        getActivity().setTitle(items.size() +" "+ itemOuItens + MoneyHelper.setTwoDecimalPlaces(purchase.getTotal()));

        View fragment = inflater.inflate(R.layout.rv_default, null);

        results = CalculatorFactory.getInstance().get(purchase).getResults();

        RecyclerView rv = fragment.findViewById(R.id.rv_default);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(new DivisionAdapter(new ArrayList<>(results)));

        return fragment;
    }
}
