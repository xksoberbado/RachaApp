package xksoberbado.br.rachaapp.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.view.adapter.ParticipantRvAdapter;
import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.arq.receiver.RefreshReceiver;
import xksoberbado.br.rachaapp.arq.view.IRefresh;
import xksoberbado.br.rachaapp.view.form.ParticipantDialog;
import xksoberbado.br.rachaapp.helper.DialogFragmentHelper;
import xksoberbado.br.rachaapp.helper.FragmentHelper;
import xksoberbado.br.rachaapp.model.Participant;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.repository.PurchaseDao;

/**
 * Created by igor.rudel on 03/07/2018.
 */

public class ParticipantsFragment extends Fragment implements IRefresh {

    private RefreshReceiver receiver;
    public static final String REFRESH = "ParticipantsFragment.REFRESH";

    private Purchase purchase;
    private RecyclerView rv;

    private ParticipantsFragment() {
    }

    public ParticipantsFragment(Purchase purchase) {
        setHasOptionsMenu(true);
        this.purchase = purchase;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_participantes, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.add_paticipante:
                DialogFragmentHelper.showFragmentDialog(new ParticipantDialog(purchase), "participant_dialog");
                return true;
            case android.R.id.home:
                FragmentHelper.replaceFragment(new PurchasesFragment());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        receiver = new RefreshReceiver(this);
        App.registerReceiver(receiver, REFRESH);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true); //APARECE O BOTÃO DE VOLTAR
        getActivity().setTitle(R.string.participants);
        View fragment = inflater.inflate(R.layout.rv_default, null);
        rv = fragment.findViewById(R.id.rv_default);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        refresh();
        return fragment;
    }

    @Override
    public void refresh() {
        new PurchaseDao().refresh(purchase);
        Collection<Participant> participants = purchase.getParticipants();
        if(!participants.isEmpty()){
            rv.setAdapter(new ParticipantRvAdapter(new ArrayList<>(participants)));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        App.unregisterReceiver(receiver);
    }
}
