package xksoberbado.br.rachaapp.view.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.view.adapter.SelectParticipantRvAdapter;

/**
 * Created by igor.rudel on 04/07/2018.
 */

public class ParticipantsDialog extends DialogFragment {

    private RecyclerView rv;
    private PurchaseItem item;

    private ParticipantsDialog() {
    }

    public ParticipantsDialog(PurchaseItem item) {
        this.item = item;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.rv_default, null);
        rv = fragment.findViewById(R.id.rv_default);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshList();
        return fragment;
    }

    private void refreshList(){
        rv.setAdapter(new SelectParticipantRvAdapter(new ArrayList<>(item.getPurchase().getParticipants()), item));
    }
}
