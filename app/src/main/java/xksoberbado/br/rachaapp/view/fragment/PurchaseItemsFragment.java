package xksoberbado.br.rachaapp.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.view.adapter.PurchaseItemRvAdapter;
import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.arq.receiver.RefreshReceiver;
import xksoberbado.br.rachaapp.arq.view.IRefresh;
import xksoberbado.br.rachaapp.view.form.PurchaseItemDialog;
import xksoberbado.br.rachaapp.helper.DialogFragmentHelper;
import xksoberbado.br.rachaapp.helper.MoneyHelper;
import xksoberbado.br.rachaapp.helper.FragmentHelper;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.repository.PurchaseDao;

/**
 * Created by Igor on 03/07/2018.
 */

public class PurchaseItemsFragment extends Fragment implements IRefresh {


    private RefreshReceiver receiver;
    public static final String REFRESH = "PurchaseItemsFragment.REFRESH";

    private Purchase purchase;
    private RecyclerView rv;

    private PurchaseItemsFragment() {
    }

    public PurchaseItemsFragment(Purchase purchase) {
        setHasOptionsMenu(true);
        this.purchase = purchase;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_items, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.menu_add_item_compra:
                DialogFragmentHelper.showFragmentDialog(new PurchaseItemDialog(purchase), "purchase_item_dialog");
                return true;
            case android.R.id.home:
                FragmentHelper.replaceFragment(new PurchasesFragment());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        receiver = new RefreshReceiver(this);
        App.registerReceiver(receiver, REFRESH);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().setTitle(getString(R.string.sale_items));
        View fragment = inflater.inflate(R.layout.rv_default, null);
        rv = fragment.findViewById(R.id.rv_default);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        refresh();

        return fragment;
    }


    @Override
    public void refresh() {
        new PurchaseDao().refresh(purchase);
        Collection<PurchaseItem> items = purchase.getItems();
        if(items != null && items.size() > 0){
            String totalItems = MoneyHelper.setTwoDecimalPlaces(purchase.getTotal()).toString();
            getActivity().setTitle(getString(R.string.sale_items) +" - "+ totalItems);
            rv.setAdapter(new PurchaseItemRvAdapter(new ArrayList<>(items)));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        App.unregisterReceiver(receiver);
    }
}
