package xksoberbado.br.rachaapp.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.view.IYesNo;
import xksoberbado.br.rachaapp.arq.view.YesNoDialog;
import xksoberbado.br.rachaapp.helper.DialogFragmentHelper;
import xksoberbado.br.rachaapp.model.Participant;
import xksoberbado.br.rachaapp.repository.ParticipantDao;
import xksoberbado.br.rachaapp.repository.PurchaseItemParticipantDao;
import xksoberbado.br.rachaapp.view.form.ParticipantDialog;

/**
 * Created by igor.rudel on 03/07/2018.
 */

public class ParticipantRvAdapter extends RecyclerView.Adapter<ParticipantRvAdapter.ViewLine> {

    private ArrayList<Participant> participants;

    private ParticipantRvAdapter() {
    }

    public ParticipantRvAdapter(ArrayList<Participant> participants) {
        this.participants = participants;
    }

    @NonNull
    @Override
    public ViewLine onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.participant_line, parent, false);
        return new ViewLine(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewLine holder, final int position) {
        final Participant p = participants.get(position);

        holder.tv.setText(p.getName());

        holder.ibEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragmentHelper.showFragmentDialog(new ParticipantDialog(p), "participant_dialog");
            }
        });

        holder.ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragmentHelper.showFragmentDialog(new YesNoDialog(R.string.participant_delete_ask, delete(p, position)), "yes_no_participant");
            }
        });
    }

    private IYesNo delete(final Participant p, final int position){
        return new IYesNo() {
            @Override
            public void yes() {
                participants.remove(position);
                new PurchaseItemParticipantDao().deleteByParticipant(p);
                new ParticipantDao().delete(p, R.string.participant_deleted);
                notifyItemRemoved(position);
            }

            @Override
            public boolean onlyDismiss() {
                return true;
            }

            @Override
            public void no() {

            }
        };
    }

    @Override
    public int getItemCount() {
        return participants.size();
    }

    public class ViewLine extends RecyclerView.ViewHolder{

        public TextView tv;
        public ImageButton ibEdit;
        public ImageButton ibDelete;

        public ViewLine(@NonNull View itemView) {
            super(itemView);
            this.tv = itemView.findViewById(R.id.tv_default);
            this.ibEdit = itemView.findViewById(R.id.ib_edit);
            this.ibDelete = itemView.findViewById(R.id.ib_delete);
        }
    }

}
