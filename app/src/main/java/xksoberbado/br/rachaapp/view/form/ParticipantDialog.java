package xksoberbado.br.rachaapp.view.form;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.App;
import xksoberbado.br.rachaapp.arq.helper.FormHelper;
import xksoberbado.br.rachaapp.view.fragment.ParticipantsFragment;
import xksoberbado.br.rachaapp.helper.ButtonHelper;
import xksoberbado.br.rachaapp.model.Participant;
import xksoberbado.br.rachaapp.model.Purchase;
import xksoberbado.br.rachaapp.repository.ParticipantDao;

/**
 * Created by igor.rudel on 03/07/2018.
 */

public class ParticipantDialog extends DialogFragment {

    private Participant participant;
    private Purchase purchase;
    private EditText etName;

    private ParticipantDialog() {
    }

    public ParticipantDialog(Purchase purchase) {
        this.purchase = purchase;
    }

    public ParticipantDialog(Participant participant) {
        this.participant = participant;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.form_participant, null);
        etName = fragment.findViewById(R.id.et_name);

        if(!isNewParticipant())
            etName.setText(participant.getName());

        Button btnSave = fragment.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    ParticipantDao dao = new ParticipantDao();
                    String name = etName.getText().toString();
                    if(isNewParticipant()){
                        dao.create(new Participant(name, purchase));
                        etName.setText("");
                    } else {
                        participant.setName(name);
                        dao.update(participant);
                    }
                    Toast.makeText(getContext(), isNewParticipant() ? R.string.participant_created : R.string.participant_updated, Toast.LENGTH_SHORT).show();
                    App.sendBroadcast(ParticipantsFragment.REFRESH);
                }
            }
        });

        ButtonHelper.setButtonCancelDialog(fragment, this);

        return fragment;
    }

    private boolean isNewParticipant(){
        return participant == null;
    }

    private boolean validate(){
        return FormHelper.validateEt(etName);
    }
}
