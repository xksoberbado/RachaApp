package xksoberbado.br.rachaapp;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import xksoberbado.br.rachaapp.arq.config.AppControl;
import xksoberbado.br.rachaapp.model.ClassMapperImpl;
import xksoberbado.br.rachaapp.view.form.PurchaseDialog;
import xksoberbado.br.rachaapp.view.fragment.PurchasesFragment;
import xksoberbado.br.rachaapp.helper.DialogFragmentHelper;
import xksoberbado.br.rachaapp.helper.FragmentHelper;

public class MainActivity extends AppCompatActivity {


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_purchase) {
            DialogFragmentHelper.showFragmentDialog(new PurchaseDialog(), "purchase_dialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppControl.start(this, new ClassMapperImpl());

        FragmentHelper.replaceFragment(new PurchasesFragment());
    }

//    private void shareImage(Bitmap bitmap){
//        try {
//            File cachePath = new File(this.getCacheDir(), "images");
//            cachePath.mkdirs(); // don't forget to make the directory
//            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//            stream.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        File imagePath = new File(this.getCacheDir(), "images");
//        File newFile = new File(imagePath, "image.png");
//        Uri contentUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", newFile);
//
//        if (contentUri != null) {
//            Intent shareIntent = new Intent();
//            shareIntent.setAction(Intent.ACTION_SEND);
//            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
//            shareIntent.setDataAndType(contentUri, getContentResolver().getType(contentUri));
//            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
//            shareIntent.setType("image/png");
//            startActivity(Intent.createChooser(shareIntent, getString(R.string.escolha_app)));
//        }
//    }
}
