package xksoberbado.br.rachaapp.repository;

import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;

import xksoberbado.br.rachaapp.arq.dao.DaoImpl;
import xksoberbado.br.rachaapp.model.Participant;
import xksoberbado.br.rachaapp.model.PurchaseItem;
import xksoberbado.br.rachaapp.model.PurchaseItemParticipant;

public class PurchaseItemParticipantDao extends DaoImpl<PurchaseItemParticipant> {

    private String ITEM_ID = "item_id";
    private String PARTICIPANT_ID = "participant_id";

    public void deleteByParticipant(Participant participant){
        try {
            DeleteBuilder deleteBuilder = this.dao.deleteBuilder();
            deleteBuilder.where().eq(PARTICIPANT_ID, participant.getId());
            this.dao.delete(deleteBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteByItem(PurchaseItem purchaseItem){
        try {
            DeleteBuilder deleteBuilder = this.dao.deleteBuilder();
            deleteBuilder.where().eq(ITEM_ID, purchaseItem.getId());
            this.dao.delete(deleteBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
