package xksoberbado.br.rachaapp.arq;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.fragment.app.FragmentManager;

import xksoberbado.br.rachaapp.MainActivity;
import xksoberbado.br.rachaapp.arq.config.AppControl;

/**
 * Created by igor.rudel on 03/07/2018.
 */

public class App {

    private App() {
    }

    public static MainActivity getActivity(){
        return AppControl.getActivity();
    }

    public static Context getContext(){
        return AppControl.getContext();
    }

    public static FragmentManager getSupportFragmentManager(){
        return AppControl.getActivity().getSupportFragmentManager();
    }

    public static void registerReceiver(BroadcastReceiver receiver, String action){
        AppControl.getContext().registerReceiver(receiver, new IntentFilter(action));
    }

    public static void sendBroadcast(String action){
        AppControl.getContext().sendBroadcast(new Intent(action));
    }

    public static void unregisterReceiver(BroadcastReceiver receiver){
        AppControl.getContext().unregisterReceiver(receiver);
    }
}
