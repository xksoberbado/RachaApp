package xksoberbado.br.rachaapp.arq.model;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public abstract class Entity implements IEntity, Serializable {

    @DatabaseField(generatedId = true, unique = true)
    protected Long id;

}
