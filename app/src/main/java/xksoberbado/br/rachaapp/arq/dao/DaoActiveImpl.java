package xksoberbado.br.rachaapp.arq.dao;

import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import xksoberbado.br.rachaapp.arq.model.ActiveEntity;

public abstract class DaoActiveImpl<E extends ActiveEntity> extends DaoImpl implements IDaoActive {

    protected String ACTIVE = "active";

    @Override
    public void enable(ActiveEntity e) {
        enable(e, null);
    }

    @Override
    public void enable(ActiveEntity e, Integer msgId) {
        e.setActive(true);
        super.update((E) e, msgId);
    }

    @Override
    public void disable(ActiveEntity e) {
        disable(e, null);
    }

    @Override
    public void disable(ActiveEntity e, Integer msgId) {
        e.setActive(false);
        super.update((E) e, msgId);
    }

    @Override
    public Collection<E> getAllEnabled() {
        return getAllByActive(true);
    }

    @Override
    public Collection<E> getAllDisabled() {
        return getAllByActive(false);
    }

    private Collection<E> getAllByActive(boolean active) {
        try {
            return this.dao.queryForEq(ACTIVE, active);
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
            return new LinkedList<>();
        }
    }
}
