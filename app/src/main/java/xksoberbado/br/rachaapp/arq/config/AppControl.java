package xksoberbado.br.rachaapp.arq.config;

import android.content.Context;

import xksoberbado.br.rachaapp.MainActivity;
import xksoberbado.br.rachaapp.arq.dao.FactoryDao;
import xksoberbado.br.rachaapp.arq.dao.IClassMapper;

public class AppControl {

    private static AppControl instance;
    private static MainActivity activity;
    private static Context context;

    private AppControl(MainActivity mainActivity) {
        this.activity = mainActivity;
        this.context = mainActivity.getApplicationContext();
    }

    public static AppControl start(MainActivity mainActivity, IClassMapper classMapper) {
        if(instance == null) {
            instance = new AppControl(mainActivity);
            FactoryDao.startDB(classMapper);
        }
        return instance;
    }

    public static MainActivity getActivity() {
        return activity;
    }

    public static Context getContext() {
        return context;
    }
}
