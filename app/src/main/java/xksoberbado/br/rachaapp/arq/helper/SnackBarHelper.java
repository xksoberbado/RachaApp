package xksoberbado.br.rachaapp.arq.helper;

import android.graphics.Color;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import xksoberbado.br.rachaapp.R;
import xksoberbado.br.rachaapp.arq.config.AppControl;

public class SnackBarHelper {

    private static String GREEN = "#238E23";
    private static String BLUE = "#00009C";
    private static String RED = "#FF0000";

    private SnackBarHelper() {
    }

    private static View getCoordinatorLayout(){
        return AppControl.getActivity().findViewById(R.id.rl_main_activity);
    }

    private static void show(int resource, int backgroundColor){
        Snackbar snackbar = Snackbar.make(getCoordinatorLayout(), resource, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(backgroundColor);
        snackbar.show();
    }

    private static void show(String text, int backgroundColor){
        Snackbar snackbar = Snackbar.make(getCoordinatorLayout(), text, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(backgroundColor);
        snackbar.show();
    }

    public static void success(int resource){
        show(resource, Color.parseColor(GREEN));
    }

    public static void danger(int resource){
        show(resource, Color.parseColor(RED));
    }

    public static void info(int resource){
        show(resource, Color.parseColor(BLUE));
    }

    public static void info(String text){
        show(text, Color.parseColor(BLUE));
    }

    public static void success(String text){
        show(text, Color.parseColor(GREEN));
    }

    public static void danger(String text){
        show(text, Color.parseColor(RED));
    }

}
