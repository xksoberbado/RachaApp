package xksoberbado.br.rachaapp.arq.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import xksoberbado.br.rachaapp.arq.helper.SnackBarHelper;

public abstract class DaoImpl<E> implements IDao<E> {

    protected String ID = "id";

    protected Dao dao;

    public DaoImpl() {
        try {
            this.dao = FactoryDao.getInstance().getDao(getClazz());
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }
    }

    private Class getClazz() {
        ParameterizedType types = (ParameterizedType) getClass().getGenericSuperclass();
        return (Class) types.getActualTypeArguments()[0];
    }

    public QueryBuilder queryBuilder(){
        return this.dao.queryBuilder();
    }

    @Override
    public void refresh(E o) {
        try {
            this.dao.refresh(o);
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }
    }

    @Override
    public void create(E o) {
       this.create(o, null);
    }

    @Override
    public void update(E o) {
        this.update(o, null);
    }

    @Override
    public void delete(E o) {
        this.delete(o, null);
    }

    @Override
    public void create(E o, Integer idMsg) {
        try {
            this.dao.create(o);
            if(idMsg != null)
                SnackBarHelper.success(idMsg);
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }
    }

    @Override
    public void update(E o, Integer idMsg) {
        try {
            this.dao.update(o);
            if(idMsg != null)
                SnackBarHelper.success(idMsg);
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }
    }

    @Override
    public void delete(E o, Integer idMsg) {
        try {
            this.dao.delete(o);
            if(idMsg != null)
                SnackBarHelper.success(idMsg);
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }
    }

    @Override
    public void deleteAll() {
        try {
            this.dao.delete(this.dao.queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
        }
    }

    @Override
    public Collection<E> getAll() {
        try {
            return this.dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
            return new LinkedList<>();
        }
    }

    @Override
    public E findById(Long id) {
        try {
            return (E) this.dao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
            return null;
        }
    }

    @Override
    public Collection<E> getAllIn(Iterable<Long> ids) {
        try {
            return this.dao.queryBuilder().where().in(ID, ids).query();
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
            return new LinkedList<>();
        }
    }

    @Override
    public long countOf() {
        try {
            return this.dao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
            showError(e.getMessage());
            return 0;
        }
    }

    protected void showError(String msg){
        SnackBarHelper.danger("BD Error:"+ msg);
    }
}
