package xksoberbado.br.rachaapp.arq.model;

import com.j256.ormlite.field.DatabaseField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class ActiveEntity extends Entity {

    @DatabaseField(canBeNull = false)
    private boolean active;

}
