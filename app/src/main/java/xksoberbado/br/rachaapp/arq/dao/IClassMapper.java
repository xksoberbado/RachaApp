package xksoberbado.br.rachaapp.arq.dao;

import java.util.HashSet;

public interface IClassMapper {

    HashSet<Class> getEntities();

}
