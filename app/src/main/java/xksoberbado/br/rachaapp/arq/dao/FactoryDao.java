package xksoberbado.br.rachaapp.arq.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashSet;

import xksoberbado.br.rachaapp.arq.App;

public class FactoryDao extends OrmLiteSqliteOpenHelper {

    private static FactoryDao instance;
    private static HashSet<Class> entities;
    private static final String DB_NAME = "RACHA_DB";
    private static final int DB_VERSION = 1;


    private FactoryDao(Context context, IClassMapper classMapper){
        super(context, DB_NAME, null, DB_VERSION);
        entities = classMapper.getEntities();
        getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            for(Class clazz : entities)
                TableUtils.createTableIfNotExists(connectionSource, clazz);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    public synchronized static void startDB(IClassMapper classMapper){
        if(instance == null) {
            instance = new FactoryDao(App.getContext(), classMapper);
            instance.onCreate(instance.getWritableDatabase(), instance.getConnectionSource());
        }
    }

    protected static FactoryDao getInstance(){
        return instance;
    }

}
