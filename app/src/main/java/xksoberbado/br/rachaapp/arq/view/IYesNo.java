package xksoberbado.br.rachaapp.arq.view;

public interface IYesNo {

    void yes();

    boolean onlyDismiss();

    void no();

}
