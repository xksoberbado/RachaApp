package xksoberbado.br.rachaapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xksoberbado.br.rachaapp.arq.model.Entity;

/**
 * Created by igor.rudel on 03/07/2018.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@DatabaseTable(tableName = "participants")
public class Participant extends Entity {

    @DatabaseField(canBeNull = false, uniqueIndexName = "purchase_participant")
    private String name;

    @DatabaseField(canBeNull = false, foreign = true, columnName = "purchase_id", foreignAutoRefresh = true, uniqueIndexName = "purchase_participant")
    private Purchase purchase;
}
