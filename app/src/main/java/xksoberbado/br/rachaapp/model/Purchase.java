package xksoberbado.br.rachaapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xksoberbado.br.rachaapp.arq.model.Entity;

/**
 * Created by igor.rudel on 03/07/2018.
 */
@NoArgsConstructor
@Getter
@Setter
@DatabaseTable(tableName = "purchases")
public class Purchase extends Entity {

    @DatabaseField(canBeNull = false)
    private Date date;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField
    private BigDecimal percentage;

    @ForeignCollectionField(eager = true)
    private Collection<PurchaseItem> items;

    @ForeignCollectionField(eager = true)
    private Collection<Participant> participants;

    public Purchase(Date date, String name, BigDecimal percentage) {
        this.date = date;
        this.name = name;
        this.percentage = percentage;
    }

    public BigDecimal getPercent(){
        return percentage.divide(new BigDecimal(100));
    }

    public BigDecimal getTotal(){
        return percentage != null ? getTotalWithPercentage() : getTotalWithoutPercentage();
    }

    private BigDecimal getTotalWithPercentage(){
        BigDecimal total = new BigDecimal(0);
        for(PurchaseItem item : items)
            total = total.add(item.getTotal());

        BigDecimal percent = getPercent();
        BigDecimal plus = total.multiply(percent);
        return total.add(plus);
    }

    private BigDecimal getTotalWithoutPercentage(){
        BigDecimal total = new BigDecimal(0);
        for(PurchaseItem item : items)
            total = total.add(item.getTotal());

        return total;
    }
}
