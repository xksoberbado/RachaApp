package xksoberbado.br.rachaapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xksoberbado.br.rachaapp.arq.model.Entity;

/**
 * Created by igor.rudel on 04/07/2018.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@DatabaseTable(tableName = "purchase_item_participant")
public class PurchaseItemParticipant extends Entity {

    @DatabaseField(canBeNull = false, foreign = true, columnName = "item_id", foreignAutoRefresh = true, uniqueIndexName = "item_participant")
    private PurchaseItem item;

    @DatabaseField(canBeNull = false, foreign = true, columnName = "participant_id", foreignAutoRefresh = true, uniqueIndexName = "item_participant")
    private Participant participant;

}
